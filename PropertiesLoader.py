# Dictionnaire contenant les données du fichier
args = {}

# === BEGIN : loadProperties() ===
def loadProperties():
    global args

    # Lecture  du fichier et récupération des données
    file = open("./properties.txt", "r")
    lines = file.readlines()
    file.close()

    # Récupération des paramètres
    for index, line in enumerate(lines):
        tmpArg = line.replace('\n', '')
        tmpArg = tmpArg.split('=')
        args[tmpArg[0]] = tmpArg[1]
# === END : loadProperties() ===

# === BEGIN : class PropertiesLoader ===
class PropertiesLoader:
    rnd_threshold = "" # Bornes de lambda
    fgen_min = "" # Hauteur minimale de la première génération
    fgen_max = "" # Hauteur maximale de la première génération
    ngen_min = "" # Hauteur minimale des générations suivantes
    ngen_max = "" # Hauteur maximale des générations suivantes
    cxpb = "" # Probabilité d'accouplement
    mutpb = "" # Probabilité de mutation
    nbgen = "" # Nombre de générations
    pop = "" # Nombre d'individus par génération

    def __init__(self):
        global args

        # Récupération des paramètres
        loadProperties()

        self.rnd_threshold = int(args["RND_THRESHOLD"])
        self.fgen_min = int(args["FGEN_MIN"])
        self.fgen_max = int(args["FGEN_MAX"])
        self.ngen_min = int(args["NGEN_MIN"])
        self.ngen_max = int(args["NGEN_MAX"])
        self.cxpb = float(args["CXPB"])
        self.mutpb = float(args["MUTPB"])
        self.nbgen = int(args["NBGEN"])
        self.pop = int(args["POP"])
# === END : class PropertiesLoader ===
