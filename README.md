# Deap

Ce projet a pour but de prendre en main l'outil **Deap** permettant de mettre en place de la programmation génétique.


## Objectifs

L'ojbectif est de trouver, ou en tout cas s'approcher le plus possible, la définition d'une fonction à partir d'un ensemble de valeurs de cette fonction. Pour cela, nous utiliserons la *régression symbolique*.

## Architecture du projet

Pour lancer le programme veuillez installer **deap** ainsi que **matplotlib**.

Dans un environnement virtuel Python :

    pip install deap
    pip install matplotlib

Pour lancer le programme :

Sous Windows : `python .\guess_funct.py\`

Sous UNIX : `python3 guess_funct.py`

### Données
Les données sont dans le fichier data.csv

### Paramètres
Les paramètres modifiables du projet sont dans le fichier properties.txt.

**ATTENTION : ne pas renommer les variables, modifier seulement les valeurs.**

- RND_THRESHOLD : initialement utilisé pour la création des variables aléatoires, inutilisé maintenant.
- FGEN_MIN : taille minimum de l'arbre pour la première génération
- FGEN_MAX : taille maximum de l'arbre pour la première génération
- NGEN_MIN : taille minimum de l'arbre pour les générations suivantes
- NGEN_MAX : taille maximum de l'arbre pour les générations suivantes
- CXPB : probabilité d'accouplement
- MUTPB : probabilité de mutation
- NBGEN : nombre de générations (sans compter la première)
- POP : population de chaque génération

### Outils

Les deux outils "DataLoader.py" et "PropertiesLoader.py" chargent les données des deux fichiers.

### Programme

Le fichier principal est "guess_funct.py".

## Approche

Nous savons qu'une fonction est généralement composée d'une ou de plusieurs inconnues. Nous savons aussi qu'un ensemble d'opérations est utilisé afin de construire cette fonction. Ces opérations sont généralement : l'addition, la soustraction, la division et la mutliplication. Nous pouvons cependant composer notre fonction avec d'autres fonctions telles que la fonction sinus ou cosinus, la fonction exponentielle, etc. A cela vient se greffer des constantes.

La programmation génétique mettant en place cette régression symbolique dans le cadre de notre projet prendra en compte les quatre opérateurs définis plus haut ainsi que des constantes.

## Programmation génétique

### Génération des individus

Les opérateurs utilisés sont :

 - Addition
 - Soustraction
 - Multiplication
 - Division protégée (si le dénominateur est null alors on retourne une grande valeur)
 - Multiplication par -1
 - Cosinus
 - Sinus

A cela, nous ajoutons des constantes comprises entre -1 et 1 et à valeur réelle *(ex : 0.354)*.

Les premiers individus sont créés avec une profondeur d'arbre comprise entre 2 et 5. Les générations suivantes sont créées avec une profondeur d'arbre comprise entre 5 et 15. Nous avons cependant limité la profondeur de l'arbre à 15 pour éviter le "bloat".

### Evaluation des individus

Les individus sont évalués sur plusieurs critères :

 - L'erreur quadratique de chaque point xi
 - L'erreur moyenne quadratique
 - L'écart type

### Sélection des individus

La sélection se fait au travers d'un tournois faisant s'affronter deux individus.

### Croisement

Les croisements se font entre deux individus selon une probabilité **CXPB**. Le croisement se fait à partir d'un sous-arbre de chaque individu.

### Mutation

La mutation se fait selon une probabilité **MUTPB**. Elle remplace un sous-arbre de l'individu par un nouvel arbre généré de la même façon qu'un individu.

## Résultats

Les résultats affichent pour chaque génération les statistiques ainsi que l'arbre du meilleur individu (souvent très peu lisible).

Le graphe de la fonction générée par l'individu est ensuite tracée grâce à **matplotlib** ainsi que les données d'entrée.

