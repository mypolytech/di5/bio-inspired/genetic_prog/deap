import math
import matplotlib.pyplot as plt
import numpy as np
import operator
import random
import sys
import tkinter

import DataLoader as dload
import PropertiesLoader as pload

from deap import algorithms
from deap import base
from deap import creator
from deap import gp
from deap import tools

# Données du problème
data = dload.loadData()
xValues = dload.loadXValues()
functValues = dload.loadFunctValues()
# Paramètres
prop = pload.PropertiesLoader()
# Valeurs du meilleur individu
result = []

# Magie noire nécessaire pour faire marcher Deap
creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin)
# Objet qui va nous carry le game
toolbox = base.Toolbox()

# === BEGIN : plotFunctions() ===
def plotFunctions():
    """
        Trace les deux courbes
    """
    # Matplotlib pyplot
    plt.scatter(xValues, functValues, label="OG", color='b')
    plt.scatter(xValues, result, label="Calculated", color='r')
    plt.xlabel('x')
    plt.ylabel("f(x)")
    plt.title("Allure des fonctions")
    plt.legend()
    plt.show()
# === END : plotFunctions() ===

# === BEGIN : protectedDiv(left, right) ===
def protectedDiv(left, right):
    try:
        return left / right
    except ZeroDivisionError:
        return 1000
# === END : protectedDiv(left, right) ===

# === BEGIN : generateIndividuals(name) ===
def generateIndividuals(name):
    """
        Génère un arbre aléatoire pour les premiers individus
    """
    pset = gp.PrimitiveSet(name, 1)

    pset.addPrimitive(operator.add, 2) # Addition
    pset.addPrimitive(operator.sub, 2) # Soustraction
    pset.addPrimitive(operator.mul, 2) # Multiplication
    pset.addPrimitive(protectedDiv, 2) # Division protégée
    pset.addPrimitive(operator.neg, 1) # Signe négatif
    pset.addPrimitive(math.cos, 1) # Cosinus
    pset.addPrimitive(math.sin, 1) # Sinus    

    pset.addEphemeralConstant("firstRand", lambda: random.random()*2 - 1)

    pset.renameArguments(ARG0="x")

    return pset
# === END : generateIndividuals(name) ===

# === BEGIN : generateToolbox(name) ===
def generateToolbox():
    """
        Instanciation de la toolbox
    """
    global functValues
    global toolbox

    # On créé le modèle des individus
    pset = generateIndividuals("first")

    # Enregistrement des paramètres
    toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=prop.fgen_min, max_=prop.fgen_max)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("compile", gp.compile, pset=pset)
    toolbox.register("evaluate", evaluateIndividual, points=functValues)
    toolbox.register("select", tools.selTournament, tournsize=2)
    toolbox.register("mate", gp.cxOnePoint)
    toolbox.register("expr_mut", gp.genFull, min_=prop.ngen_min, max_=prop.ngen_max)
    toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

    # On empêche le "bloat"
    toolbox.decorate("mate", gp.staticLimit(key=operator.attrgetter("height"), max_value=15))
    toolbox.decorate("mutate", gp.staticLimit(key=operator.attrgetter("height"), max_value=15))
# === END : generateToolbox(name) ===

# === BEGIN : evaluateIndividual() ===
def evaluateIndividual(individual, points):
    """
        Evaluation de la fonction générée par l'individu
    """
    global toolbox

    # Transformation de l'arbre en une fonction
    function = toolbox.compile(expr=individual)

    length = len(points) # Nombre de valeurs
    tmpRes = [] # Tableau contenant tous les function(i)
    tuple = () # Les valeurs qui seront minimisées

    # Calcul de tous les function(i)
    for i in range(0, length):
        tmpRes.append(function(i))

    # Erreur moyenne quadratique
    mse = 0

    # Erreur quadratique
    for i in range(0, length):
        try:
            se = (function(i) - points[i])**2
            mse += se
            tmpRes.append(se)
        except:
            tmpRes = sys.float_info.max # Le carré peut faire péter les scores

        tuple = tuple + (se,)

    tuple += tuple + (mse/length,)

    # Ecart type des erreurs
    stdDev = np.std(tmpRes)
    tuple += (stdDev,)

    return tuple
# === END : evaluateIndividual() ===

# === BEGIN : main ===
if __name__ == "__main__":
    # Création de la toolbox
    generateToolbox()

    # Objets pour calculer les indicateurs statistiques
    stats_fit = tools.Statistics(lambda ind: ind.fitness.values)
    mstats = tools.MultiStatistics(fitness=stats_fit)
    mstats.register("avg", np.average)
    mstats.register("std", np.std)

    # Création de la routine
    pop = toolbox.population(n=prop.pop)
    hof = tools.HallOfFame(1)

    cxpb = prop.cxpb # Probabilité d'accouplement
    mutpb = prop.mutpb # Probabilité de mutation
    ngen = prop.nbgen # Nombre de gnération

    # Lancement de la routine
    pop, log = algorithms.eaSimple(pop, toolbox, cxpb, mutpb, ngen, stats=mstats, halloffame=hof, verbose=True)

    # Affichage du meilleur arbre
    print("\n" + str(hof[0]) + "\n")

    # On range les résultats dans un tableau
    calculation = toolbox.compile(expr=hof[0])

    for x in xValues:
        result.append(float(calculation(x)))

    # On trace les deux courbes
    plotFunctions()
# === END : main ===
