data = []
xValues = []
functValues = []

# === BEGIN : initData() ===
def initData():
    """
        Initialise le tableau data
    """
    global data

    if not data:
        # Lecture  du fichier et récupération des données
        file = open("./data.csv", "r")
        lines = file.readlines()
        file.close()

        # On récupère toutes les lignes et on les split
        for line in lines:
            line = line.replace('\n', '')
            line = line.split(',')
            data.append(line)
# === END : initData() ===

# === BEGIN : loadData() ===
def loadData():
    """
        Charge le contenu du fichier "data.csv"
    """
    global data

    initData()

    return data
# === END : loadData() ===

# === BEGIN : loadXValues() ===
def loadXValues():
    """
        Récupère les valeurs de x
    """
    global data
    global xValues

    # Sécurité
    if not data:
        initData()

    # Nombre de valeurs
    length = len(data)

    for i in range(0, length):
        if(len(data[i]) > 1):
            xValues.append(float(data[i][0]))

    return xValues
# === END : loadXValues() ===

# === BEGIN : loadFunctValues() ===
def loadFunctValues():
    """
        Récupère les valeurs de f(x)
    """
    global data
    global functValues

    # Sécurité
    if not data:
        initData()

    # Nombre de valeurs
    length = len(data)

    for i in range(0, length):
        if(len(data[i]) > 1):
            functValues.append(float(data[i][1]))

    return functValues
# === FIN : loadFunctValues() ===
